
package GameObj;


public interface IGame extends IGameStream {
    
    public void setGameID(int id);
    
    public int getGameID();
    
    public void setAwayTeam(String team);
    
    public String getAwayTeam();
    
    public void setHomeTeam(String team);
    
    public String getHomeTeam();
    
    public void setAwayTeamFull(String team);
    
    public String getAwayTeamFull();
    
    public void setHomeTeamFull(String team);
    
    public String getHomeTeamFull();
    
    public void setTime(String time);
    
    public String getTime();

    public void setTimeRemaining(String timeRemaining);
    
    public String getTimeRemaining();
    
    public void setGameState(String state);
    
    public String getGameState();
}
